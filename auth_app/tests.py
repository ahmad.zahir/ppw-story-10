from django.test import TestCase
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from django.urls import resolve, reverse
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from django.contrib.auth import login
from django.contrib.auth.models import User
# Create your tests here.
class Story10UnitTest(TestCase):
    def testIfLandingPageUrlExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testIfLandingPageUsesIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def testIfLandingPageUsesIndexhtml(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testUserModelCreation(self):
        user = User.objects.create_user('Ahmad', 'ahmad@ui.ac.id', 'pass123')
        user.save()
        self.assertEqual(User.objects.count(), 1)

    def testIfHomePageUrlRedirectsToIndexUrlBeforeLogin(self):
        response = self.client.get('/home/')
        self.assertRedirects(response, '/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)

    def testIfLoginFromIndexRedirectsToHomeUrlAfterLogin(self):
        user = User.objects.create_user('Ahmad', 'ahmad@ui.ac.id', 'pass123')
        user.save()
        response = self.client.post('/', {'username' : 'Ahmad', 'password' : 'pass123'})
        self.assertRedirects(response, '/home/', status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)

    def testIfHomePageUsesHomehtmlAfterLogin(self):
        user = User.objects.create_user('Ahmad', 'ahmad@ui.ac.id', 'pass123')
        user.save()
        self.client.post('/', {'username' : 'Ahmad', 'password' : 'pass123'})
        response = self.client.get('/home/')
        self.assertTemplateUsed(response, 'home.html')

    def testIfHomePageUsesHomeFunc(self):
        found = resolve('/home/')
        self.assertEqual(found.func, views.home)

    def testIfSignupPageUrlExist(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code, 200)

    def testIfSignupPageUsesSignupFunc(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, views.signup)

    def testIfSignPageUsesSignuphtml(self):
        response = self.client.get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

class Story10FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        user = User.objects.create_user('Ahmad', 'ahmad@ui.ac.id', 'pass123')
        user.save()

        self.browser = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        super().tearDown()
        self.browser.quit()

    def testIfIndexPageTitleContainTextHomepage(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn('Homepage', self.browser.title)

    def testForLoginUsingDummyUserChangedBrowserTitleToHomeTitle(self):
        self.browser.get(self.live_server_url + '/')
        login_form = self.browser.find_element_by_xpath("//form[@id='login-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='username']")
        password_box = login_form.find_element_by_xpath("//input[@name='password']")
        username_box.send_keys('Ahmad')
        password_box.send_keys('pass123')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn('Logged in as Ahmad', self.browser.title)

    def testLogoutButtonChangedBrowserTitleToIndexTitle(self):
        self.browser.get(self.live_server_url + '/')
        login_form = self.browser.find_element_by_xpath("//form[@id='login-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='username']")
        password_box = login_form.find_element_by_xpath("//input[@name='password']")
        username_box.send_keys('Ahmad')
        password_box.send_keys('pass123')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        logout_btn = self.browser.find_element_by_xpath("//form[@id='logout']//button[@name='logout']")
        time.sleep(1)
        logout_btn.click()
        time.sleep(1)
        self.assertIn('Homepage', self.browser.title)

    def testForInvalidLoginCredentialsShowErrorMessage(self):
        self.browser.get(self.live_server_url + '/')
        login_form = self.browser.find_element_by_xpath("//form[@id='login-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='username']")
        password_box = login_form.find_element_by_xpath("//input[@name='password']")
        username_box.send_keys('Ahmad')
        password_box.send_keys('pass')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn('Invalid username or password.', self.browser.find_element_by_xpath("//p[@id='error-msg']").text)

    def testForLoginAfterValidSignUp(self):
        self.browser.get(self.live_server_url + '/signup/')
        login_form = self.browser.find_element_by_xpath("//form[@id='signup-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='signup-username']")
        password_box = login_form.find_element_by_xpath("//input[@name='signup-password']")
        confirm_password_box = login_form.find_element_by_xpath("//input[@name='signup-confirm-password']")
        username_box.send_keys('Rabbani')
        password_box.send_keys('coba123')
        confirm_password_box.send_keys('coba123')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        self.browser.get(self.live_server_url + '/')
        login_form = self.browser.find_element_by_xpath("//form[@id='login-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='username']")
        password_box = login_form.find_element_by_xpath("//input[@name='password']")
        username_box.send_keys('Rabbani')
        password_box.send_keys('coba123')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn('Logged in as Rabbani', self.browser.title)

    def testForInvalidSignupDifferentPasswordAndPasswordConfirmation(self):
        self.browser.get(self.live_server_url + '/signup/')
        login_form = self.browser.find_element_by_xpath("//form[@id='signup-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='signup-username']")
        password_box = login_form.find_element_by_xpath("//input[@name='signup-password']")
        confirm_password_box = login_form.find_element_by_xpath("//input[@name='signup-confirm-password']")
        username_box.send_keys('siapaini')
        password_box.send_keys('coba456')
        confirm_password_box.send_keys('coba789')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        error = self.browser.find_element_by_xpath("//p[@id='error']").text
        self.assertIn('Error', error)


    def testForInvalidSignupUsernameAlreadyRegistered(self):
        self.browser.get(self.live_server_url + '/signup/')
        login_form = self.browser.find_element_by_xpath("//form[@id='signup-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='signup-username']")
        password_box = login_form.find_element_by_xpath("//input[@name='signup-password']")
        confirm_password_box = login_form.find_element_by_xpath("//input[@name='signup-confirm-password']")
        username_box.send_keys('ppw-story')
        password_box.send_keys('coba456')
        confirm_password_box.send_keys('coba456')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        self.browser.get(self.live_server_url + '/signup/')
        login_form = self.browser.find_element_by_xpath("//form[@id='signup-form']")
        username_box = login_form.find_element_by_xpath("//input[@name='signup-username']")
        password_box = login_form.find_element_by_xpath("//input[@name='signup-password']")
        confirm_password_box = login_form.find_element_by_xpath("//input[@name='signup-confirm-password']")
        username_box.send_keys('ppw-story')
        password_box.send_keys('coba6')
        confirm_password_box.send_keys('coba6')
        password_box.send_keys(Keys.RETURN)
        time.sleep(2)
        error = self.browser.find_element_by_xpath("//p[@id='error']").text
        self.assertIn('Error', error)

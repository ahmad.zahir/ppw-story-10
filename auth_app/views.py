from django.shortcuts import render
from django.contrib.auth import authenticate, login, models, logout
from django.shortcuts import redirect

# Create your views here.
def index(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            return render(request, 'index.html', {'msg' : 'Invalid username or password. Please check it again!'})
    return render(request, 'index.html')

def home(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            logout(request)
            return redirect('index')
        return render(request, 'home.html',{'name' : request.user.username})
    else:
        return redirect('index')

def signup(request):
    if request.method=='POST':
        try:
            if(request.POST['signup-password'] != request.POST['signup-confirm-password']):
                raise Exception
            user = models.User.objects.create_user(username=request.POST['signup-username'],password=request.POST['signup-password'])
            user.save()
        except:
            return render(request, 'signup.html', {'msg': 'Error occured while creating user. If problem persists try using different username or make sure that password and confirm password are the same.'})
        return redirect('index')
    return render(request, 'signup.html')
